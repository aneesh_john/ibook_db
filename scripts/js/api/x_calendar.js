var API = API || {};

API.x_calendar = {
    CalendarManager: function(items, language, preview, formTrigger) {

        /**
         * @param items             reservations on rooms
         * @param language          translation array
         * @param preview           is form submit
         * @param trigger           prices trigger
         */

        var parent = this;
        var reservations = $.parseJSON(items);
        var limit = 1;                      // date picker today date (0 -  is available, 1 - not available)
        var calendar = "#calendar";         // element to attach calendar
        var check_in_id = "#check_in";      // element to attach date picker
        var check_out_id = "#check_out";    // element to attach date picker
        var isPreview = preview             // is form submit or not - currently not used
        var trigger = formTrigger           // prices trigger

        this.Data = {
            StartDate: null,
            EndDate: null
        }

        this.CurrentReservation = {
            StartDate: null,
            EndDate: null,
            Set: function(start, end, switchDate) {

                if (switchDate === true) {

                    if (start > end) {
                        var tmp = start;
                        start = end;
                        end = tmp;
                    }

                    var nextDayDate = parent.DatePicker.NextDayDate(start, 1);
                    parent.DatePicker.SetMinDate(check_out_id, nextDayDate);

                    parent.DatePicker.SetDate(check_in_id, start);
                    parent.DatePicker.SetDate(check_out_id, end);

                    $(trigger).trigger('change'); // Calculation

                }
                this.StartDate = start;
                this.EndDate = end;

            },
            Clear: function() {
                this.StartDate = null;
                this.EndDate = null;
            },
            IfClear: function() {
                return this.StartDate === null;
            },
            IfEqual: function() {
                return this.StartDate === this.EndDate;
            }

        }

        this.Calendar = {
            OnSelect: function(dateStr, init) {

                var date = parent.Calendar.GetCalendarDate();

                if (!parent.CurrentReservation.IfEqual()) {
                    parent.CurrentReservation.Set(date, date, false);
                } else {
                    parent.CurrentReservation.Set(parent.CurrentReservation.StartDate, date, true);
                }

            },
            BeforeShowDay: function(date) {

                var isReserved = true;
                var reservedStyle = "";
                var reservedTooltip = language.available;

                /*
                 * Highlight Reservations
                 */
                if (reservations)
                    $.each(reservations, function(key, value) {

                        var startDate = new Date(value.start);
                        var endDate = new Date(value.end);

                        if (startDate.getTime() <= date.getTime() && date.getTime() <= endDate.getTime()) {

                            if (reservedStyle !== "") {

                                reservedStyle = 'zauzeto';
                                reservedTooltip = language.occupied + ' spoj';

                            } else if (startDate.getTime() === date.getTime()) {

                                reservedStyle = 'zauzeto_dolazak';
                                reservedTooltip = language.occupied + ' ' + language.arrival;

                            } else if (endDate.getTime() === date.getTime()) {

                                reservedStyle = 'zauzeto_odlazak';
                                reservedTooltip = language.occupied + ' ' + language.departure;

                            } else {

                                reservedStyle = 'zauzeto';
                                reservedTooltip = language.occupied;

                            }

                            return false;

                        }

                    });

                /*
                 * Highlight selected
                 */
                var startDate = new Date(parent.CurrentReservation.StartDate);
                var endDate = new Date(parent.CurrentReservation.EndDate);

                if (startDate.getTime() <= date.getTime() && date.getTime() <= endDate.getTime()) {

                    if (startDate.getTime() === date.getTime()) {

                        reservedStyle += ' trenutno_dolazak ';
                        reservedTooltip = language.selected;

                    } else if (endDate.getTime() === date.getTime()) {

                        reservedStyle += ' trenutno_odlazak ';
                        reservedTooltip = language.selected;

                    } else if (startDate.getTime() < date.getTime() && date.getTime() < endDate.getTime()) {

                        reservedStyle += ' trenutno ';
                        reservedTooltip = language.selected;

                    }

                }

                return [isReserved, reservedStyle, reservedTooltip];

            },
            GetCalendarDate: function() {
                return $(calendar).datepicker('getDate');
            },
            SetCalendarDate: function(date) {
                return $(calendar).datepicker('setDate', date);
            },
            Initialize: function() {

                $(calendar).datepicker({
                    minDate: parent.DatePicker.NextDayDate(new Date(), limit),
                    maxDate: new Date(new Date().getFullYear() + 1, 11, 31),
                    numberOfMonths: 3,
                    showStatus: true,
                    inline: true,
                    showOtherMonths: false,
                    beforeShowDay: this.BeforeShowDay,
                    onSelect: parent.Calendar.OnSelect
                });

            }

        };

        this.DatePicker = {
            OnSelect: function(dateStr) {

                var arrival = parent.DatePicker.GetDate(check_in_id);
                var departure = parent.DatePicker.GetDate(check_out_id);

                if (arrival >= departure) {

                    var nextDayDate = parent.DatePicker.NextDayDate(arrival, 1);

                    parent.DatePicker.SetMinDate(check_out_id, nextDayDate);
                    parent.DatePicker.SetDate(check_out_id, nextDayDate);

                    parent.Calendar.SetCalendarDate(nextDayDate);

                    departure = parent.DatePicker.GetDate(check_out_id);

                } else {
                    parent.DatePicker.SetMinDate(check_out_id, parent.DatePicker.NextDayDate(arrival, 1));

                }

                parent.CurrentReservation.Set(arrival, departure, false);
                parent.Calendar.SetCalendarDate(arrival);

                $(trigger).trigger('change'); // Calculation

            },
            SetDate: function(ID, date) {
                $(ID).datepicker('setDate', date);
            },
            SetMinDate: function(ID, date) {
                $(ID).datepicker('option', 'minDate', date);
            },
            GetDate: function(ID, format) {
                return $(ID).datepicker('getDate');
            },
            NextDayDate: function(date, noDays) {

                myDate = new Date(date);

                myDate.setDate(myDate.getDate() + noDays);
                return myDate;
            }
        }

        this.Initialize = function() {

            this.Calendar.Initialize();






            init_pickers = function(picker2, picker1) {

                var limitFromToday = 2;
                var myPicker2 = new API.x_picker.DateManager(picker2, limitFromToday);
                myPicker2.Initialize();

                var limitFromToday = 1;
                var myPicker1 = new API.x_picker.DateManager(picker1, limitFromToday, myPicker2);
                myPicker1.Initialize();
            };

            init_pickers(check_out_id, check_in_id);


            $('.newcalendar').addClass('admin-color-clean');
            $('.newcalendar').addClass('admin-color-clean');




            // Initialize Date pickers inputs and attach on change event :: api/form.js
//            API.Form.DatePicker.Init(check_in_id, check_out_id, 1);

            // Attach on change event on date pickers inputs
            $(check_in_id).datepicker('option', {onSelect: this.DatePicker.OnSelect});
            $(check_out_id).datepicker('option', {onSelect: this.DatePicker.OnSelect});

            // Selected dates
            this.CurrentReservation.StartDate = $(check_in_id).datepicker('getDate');
            this.CurrentReservation.EndDate = $(check_out_id).datepicker('getDate');
            this.DatePicker.OnSelect(false);

        }

    }

};