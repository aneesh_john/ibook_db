var API = API || {};

API.x_autocomp = {
    AutocompManager: function(x_input, url, show_all_items) {

        /**
         * @param x_input       input to attach date picker on
         */

        var input = x_input;

        var myUrl = typeof url !== 'undefined' ? url : API.Data.base_url + "destinations_hotels_JSON";
        var myShowAll = typeof show_all_items !== 'undefined' ? show_all_items : false;

        this.Initialize = function() {

            $(input).autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: myUrl,
                        dataType: "jsonp",
                        data: {
                            featureClass: "P",
                            style: "full",
                            maxRows: 9,
                            name_startsWith: request.term
                        },
                        success: function(data) {
                            response($.map(data.items, function(item) {

                                return {
                                    label: item.name + (item.category === "child" ? " / " + item.parent : ""),
                                    value: item.name,
                                    category: item.category
                                };



                            }));
                        }
                    });
                },
                minLength: 0
            }).addClass("ui-widget ui-widget-content ui-corner-left").
                    data("ui-autocomplete")._renderItem = function(ul, item) {
                return $('<li class="ui-menu-item-with-icon"></li>')
                        .data("item.autocomplete", item)
                        .append('<a><span class="' + item.category + '-item-icon"></span>' + item.label + '</a>')
                        .appendTo(ul);
            };

            if (myShowAll) {
                $("<a href='javascript:'>&nbsp;</a>")
                        .attr("title", myShowAll)
                        .insertAfter($(input))
                        .addClass("custom-combo-link clear-border-left")
                        .click(function() {
                            // close if already visible                         
                            if ($(input).autocomplete("widget").is(":visible")) {
                                $(input).autocomplete("close");
                                return;
                            }
                            $(this).blur();
                            $(input).val("");
                            $(input).autocomplete("search", "");
                            $(input).focus();
                        });
            }

        };

    }
};