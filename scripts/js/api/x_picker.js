var API = API || {};

API.x_picker = {
    DateManager: function(x_input, x_limit, x_picker, dont_set) {

        /**
         * @param x_input       input to attach date picker on
         */

        var input = x_input;
        var limit = 0;
        var parent = this;
        var controlPicker = false;
        var onlyMin = true;

        if (typeof x_limit !== 'undefined')
            limit = x_limit;

        if (typeof x_picker !== 'undefined')
            controlPicker = x_picker;
        
        if (typeof dont_set !== 'undefined')
            onlyMin = dont_set;

        this.Data = {
            initialized: false,
            message: '',
            date: new Date(),
            image: API.Data.base_url + "assets/img/calendar.png",
            limit: limit

        };

        this.SetDate = function(date) {
            $(input).datepicker('setDate', date);
        };
        this.SetMinDate = function(date) {
            $(input).datepicker('option', 'minDate', date);
        };
        this.GetDate = function() {
            return $(input).datepicker('getDate');
        };
        this.NextDayDate = function(date, noDays) {
            var myDate = new Date(date);
            myDate.setDate(myDate.getDate() + noDays);
            return myDate;
        };

        this.OnSelect = function(dateStr) {

            if (!controlPicker)
                return;

            var arrival = parent.GetDate();
            var departure = controlPicker.GetDate();

            controlPicker.SetMinDate(parent.NextDayDate(arrival, 1));

            if (arrival >= departure && onlyMin)
                controlPicker.SetDate(parent.NextDayDate(arrival, 1));

        };

        this.Initialize = function(minDate) {

            minDate = typeof minDate !== 'undefined' ? minDate : this.NextDayDate(new Date(), this.Data.limit);

            $(input).datepicker({
                minDate: minDate,
                dateFormat: 'dd.mm.yy',
                showOn: 'both',
                buttonImage: this.Data.image,
                buttonImageOnly: true,
                nextText: '&raquo;',
                prevText: '&laquo;',
                showOtherMonths: true,
                firstDay: 1, // Start with Monday
                onSelect: this.OnSelect
            });

            $("#ui-datepicker-div").addClass("newcalendar");
            $("#ui-datepicker-div").addClass("singleCalendar");
            $("#ui-datepicker-div").addClass("ll-skin-latoja");

        };


    }
};