<?php

    /* -----------------------------------------------------------------------
      Titles
      ----------------------------------------------------------------------- */

    $lang['accommodation'] = 'Accommodation';
    $lang['accommodations'] = 'Accommodations';
    $lang['destination'] = 'Destination';
    $lang['destinations'] = 'Destinations';

    /* -----------------------------------------------------------------------
      Forms Accommodation
      ----------------------------------------------------------------------- */

    $lang['check_in'] = 'Check in';
    $lang['check_out'] = 'Check out';
    $lang['number_of_guests'] = 'Number of guests';
    $lang['ages'] = 'Ages';
    
    $lang['accommodation_search'] = 'Accommodation search';
    $lang['destination_hotel'] = 'Destination or Hotel name';
    $lang['destination_region'] = 'Destination or Region';
    $lang['destination_accomm'] = 'Destination / Accomm';
    $lang['flexible_dates'] = 'My dates are flexible';
    $lang['search'] = 'Search';


    /* -----------------------------------------------------------------------
      Currencies
      ----------------------------------------------------------------------- */

    $lang['EUR'] = '&#128;';
    $lang['RUB'] = ' RUB ';
    $lang['USD'] = '$';


    /* -----------------------------------------------------------------------
      Accommodation Types
      ----------------------------------------------------------------------- */

    $lang['apartment'] = 'apartment';
    $lang['hotel'] = 'hotel';
    $lang['villa'] = 'villa';
    $lang['authentic accommodation'] = 'authentic accommodation';
    $lang['youth hostel'] = 'youth hostel';
    $lang['houses with pool'] = 'houses with pool';
    $lang['rooms'] = 'rooms';


    /* -----------------------------------------------------------------------
      Room Types
      ----------------------------------------------------------------------- */

    $lang['t_apartment'] = "Apartment";
    $lang['t_single_room'] = "Single room";
    $lang['t_double_room'] = "Double room";
    $lang['t_triple_room'] = "Triple room";
    $lang['t_four-bedded_room'] = "Four-bedded Room";
    $lang['t_family_room'] = "Family room";
    $lang['t_junior_suite'] = "Junior suite";
    $lang['t_bungalow'] = "Bungalow";
    $lang['t_room'] = "Room";
    $lang['t_twin_room'] = "Twin room";
    $lang['t_studio'] = "Studio";
    $lang['t_superior_room'] = "Superior room";
    $lang['t_premium_room'] = "Premium room";
    $lang['t_suite'] = "Apartment (suite)";
    $lang['t_cottage'] = "Cottage";
    $lang['t_executive_suite'] = "Executive suite";
    $lang['t_presidential_suite'] = "Presidential suite";
    $lang['t_penthouse'] = "Penthouse apartman";
    $lang['t_luxury_suite'] = "Luxury suite";
    $lang['t_residence'] = "Residence";
    $lang['t_deluxe'] = "Deluxe";


    /* -----------------------------------------------------------------------
      Service type
      ----------------------------------------------------------------------- */

    $lang["per person/day"] = "per person/day";
    $lang["per person/night"] = "per person/night";
    $lang["per item per day"] = "per item per day";
    $lang["per room/night"] = "per room/night";


    /* -----------------------------------------------------------------------
      Accommodation Sorter
      ----------------------------------------------------------------------- */

    $lang['sort_by'] = 'Sort by';
    $lang['recommended'] = 'Recommended';
    $lang['stars'] = 'Stars';
    $lang['price'] = 'Price';
    $lang['results'] = 'Results';


    /* -----------------------------------------------------------------------
      Accommodation Filter
      ----------------------------------------------------------------------- */

    $lang['filter_your_results'] = 'Filter your results';
    $lang['accommodation_type'] = 'Accommodation type';
    $lang['star_rating'] = 'Star rating';
    $lang['review_score'] = 'Review Score';
    $lang['meals'] = 'Meals';
    $lang['object_facility'] = 'Object facility';
    $lang['distance_from_beach'] = 'Distance from beach';
    $lang['prices'] = 'Prices';

    $lang['star_1'] = '1 Star';
    $lang['star_2'] = '2 Stars';
    $lang['star_3'] = '3 Stars';
    $lang['star_4'] = '4 Stars';
    $lang['star_5'] = '5 Stars';
    $lang['star_0'] = 'No rating';

    $lang['score_5'] = 'Average: 5+';
    $lang['score_6'] = 'Pleasant: 6+';
    $lang['score_7'] = 'Good: 7+';
    $lang['score_8'] = 'Very good: 8+';
    $lang['score_9'] = 'Wonderful: 9+';
    $lang['score_10'] = 'Exceptional: 10';
    $lang['score_0'] = 'No rating';

    $lang['less_than_100_m'] = 'Less than 100 m';
    $lang['less_than_500_m'] = 'Less than 500 m';
    $lang['less_than_2_km'] = 'Less than 2 km';
    $lang['less_than_10_km'] = 'Less than 10 km';

    /* -----------------------------------------------------------------------
      Accommmodation API Box
      ----------------------------------------------------------------------- */

    $lang['service'] = 'Service';
    $lang['total_price'] = 'Total Price';
    $lang['night'] = 'night';
    $lang['nights'] = 'nights';
    $lang['person'] = 'person';
    $lang['persons'] = 'persons';
    $lang['book_now'] = 'Book now';
    $lang['send_request'] = 'Send request';
    $lang['price_for'] = 'price for';
    $lang['for'] = 'for';


    /* -----------------------------------------------------------------------
      Accommodation Results
      ----------------------------------------------------------------------- */

    $lang['from'] = 'from';
    $lang['search_results'] = 'Search results';
    $lang['no_accommodation_found'] = 'No accommodation found, please change your settings';
    $lang['no_excursion_found'] = 'No excursion found, please change your settings';
    $lang['show_more_rooms'] = 'Show more rooms';
    $lang['hide_rooms'] = 'Hide rooms';


    /* -----------------------------------------------------------------------
      Accommodation Details
      ----------------------------------------------------------------------- */

    $lang['total'] = 'Total';
    $lang['description'] = 'Description';
    $lang['information'] = 'Information';
    $lang['prices_availability'] = 'Prices & Availability';
    $lang['prices_booking'] = 'Prices & Booking';
    $lang['map_distances'] = 'Map & Distances';
    $lang['user_review'] = 'Guest reviews';
    $lang['room'] = 'Room';
    $lang['accepted_credit_cards'] = 'Accepted credit cards';
    $lang['pets'] = 'Pets';
    $lang['other_accommodation_units'] = 'Other accommodation units';
    $lang['view_details'] = 'View details';
    $lang['distances_from'] = 'Distances from';
    $lang['map'] = 'Map';
    $lang['define_date'] = 'Define the date of your stay';
    $lang['define_arrival'] = 'Date of arrival';
    $lang['define_departure'] = 'Date of departure';
    $lang['no_persons'] = 'Please enter the number of persons';
    $lang['available'] = 'Available';
    $lang['occupied'] = 'Occupied';
    $lang['selected'] = 'Selected';
    $lang['arrival'] = 'arrival';
    $lang['departure'] = 'departure';
    $lang['instructions'] = 'Instructions: With the 1st click on the field marked date select the beginning date of your vacation. With the 2nd click select the end date of your vacation.';
    $lang['cal_info'] = "The calendar is for informational purposes only, send a request to agency.";
    $lang['calculation'] = 'Calculation';
    $lang['total'] = 'Total';
    $lang['calculating'] = 'Calculating';
    $lang['num_of_persons'] = 'Number of persons';
    $lang['adults'] = 'adults';
    $lang['teenagers'] = 'teenagers';
    $lang['children'] = 'children';
    $lang['babies'] = 'babies';
    $lang['period'] = 'Period';
    $lang['information'] = 'Information';
    $lang['tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['tax_not_included'] = 'Residential tax and insurance are NOT included in the price and are being paid at check-in.';
    $lang["city_tax"] = "Residential tax";
    $lang["insurance_tax"] = "Insurance";
    $lang["eco_tax"] = "Eco tax";
    $lang["show_on_map"] = "Show on map";

    /* -----------------------------------------------------------------------
      Available messages
      ----------------------------------------------------------------------- */

    $lang['not_available'] = 'Room is not available in this period!';
    $lang['too_meny_person'] = 'You have selected too many persons!';
    $lang['min_stay'] = 'Minimum stay';
    $lang['min_ps'] = 'Minimum persons is ';
    $lang['days'] = 'nights';
    $lang['person'] = 'person';


    /* -----------------------------------------------------------------------
      Accommodation Checkout
      ----------------------------------------------------------------------- */

    $lang['personal_info'] = 'Personal information';
    $lang['contact_info'] = 'Contact information';
    $lang['spec_request'] = 'Special request';
    $lang['spec_req_text'] = 'Do you have any special requests for your stay? Your request will be received when you completed booking.';
    $lang['res_info'] = 'Reservation details';
    $lang['gender'] = 'Gender';
    $lang['male'] = 'Male';
    $lang['female'] = 'Female';
    $lang['date_of_birth'] = 'Date of birth';
    $lang['mobile_phone'] = 'Mobile phone';
    $lang['city'] = 'City';
    $lang['address'] = 'Address';
    $lang['zip_code'] = 'ZIP Code';
    $lang['c.methods_of_payment'] = 'Methods of payment';
    $lang['c.cc_online'] = 'Credit card online';
    $lang['c.cc_offline'] = 'Credit card using offline authorization';
    $lang['c.bank_trasfer'] = 'Bank transfer in EUR';
    $lang['c.wu'] = 'Western Union';
    $lang['c.payment_in_full'] = 'Payment in full';
    $lang['c.30_70'] = '30% now and 70% balance upon arrival';
    $lang['c.30_70_10'] = '30% now and 70% balance 10 days before arrival';
    $lang['c.i_have_read'] = 'I have read and accept the';
    $lang['c.general_terms'] = 'general terms';
    $lang['c.conditions'] = 'conditions';
    $lang['make_reservation'] = 'Make Reservation';
    $lang['make_request'] = 'Make Request';
    $lang['advance_payment'] = 'Advance payment';
    $lang['name'] = 'Name';
    $lang['surname'] = 'Surname';
    $lang['country'] = 'Country';
    $lang['email'] = 'E-mail';
    $lang['mobile'] = 'Mobile phone';
    $lang['amount'] = 'Amount';
    $lang['no_nights'] = 'Number of nights';
    $lang["day"] = "day";
    $lang['c.confirmation'] = 'Confirmation';
    $lang['thanx_inquery_sent'] = 'Inquiry sent';
    $lang['thanx_text'] = 'Thank you for your inquiry.';
    $lang['thanx_realtime_sent'] = 'Reservation Confirmed';
    $lang['thanx_realtime_text'] = 'Thank you for your reservation.';
    $lang['thanx_payment_text'] = 'Thank you for your reservation.<br><br>For any additional questions or information, please contact our customer service.';
    $lang["pricelist"] = "Price list";
    $lang["price_in"] = "Price in";
    $lang["mindays"] = "Min. Days";
    $lang["minpersons"] = "Min. Persons";
    $lang['vat'] = 'The Price includes VAT (19%)';


    /* -----------------------------------------------------------------------
      Excursions
      ----------------------------------------------------------------------- */

    $lang['excursions'] = 'Excursions';
    $lang['excursion'] = 'Excursion';
    $lang['interest'] = 'Interest';
    $lang['tags_info'] = 'e.g. History & Culture, Nature, Sport & Adventure';
    $lang['departure_date'] = 'Departure date';
    $lang['number_of_persons'] = 'Number of persons';
    $lang['per_person'] = 'per person';
    $lang['start_day'] = 'Departure day';
    $lang['guides'] = 'Guides';
    $lang['transportation_type'] = 'Transportation type';
    $lang['addition'] = 'Addition';
    $lang['pickup_location'] = 'Pickup Location';
    $lang['description'] = 'Description';
    $lang['exc_note'] = '<p>Prices are valid for summer season.</p><p>In the off season please contact agency for prices details.</p>';
    $lang['exc_similar'] = 'Similar excursions';
    $lang['exc_cal_instructions'] = 'Instructions: Click on  calendar to select the date of departure.';
    $lang['exc_departure_day'] = 'Please choose departure day';
    $lang['exc_service'] = 'Service';
    $lang['please_select'] = '-- Please select';
    $lang['week_days'] = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $lang['transports'] = array('Bus', 'Boat', 'Jeep', 'Bus + Boat', 'Bus + Jeep', 'Bus + Canoe');
    $lang['places_left'] = 'The remaining places ';
    $lang['exc_autocomp'] = 'History & Culture, Nature, Sport & Adventure';
    $lang['sellingSeason'] = 'Selling season: ';
    $lang['exc_category'] = 'Category';


    /* -----------------------------------------------------------------------
      Transfers
      ----------------------------------------------------------------------- */

    $lang['transfer'] = 'Transfer';
    $lang['find_transfer'] = 'Find transfer';
    $lang['date'] = 'Date';
    $lang['date_return'] = 'Return date';
    $lang['time'] = 'Time';
    $lang['book_a_return'] = 'Book a return';
    $lang['ban_intro'] = 'Book your transfer on time, in just a few minutes, with a reliable partner';
    $lang['ban_1'] = 'No waiting in line for a taxi';
    $lang['ban_2'] = 'Simple, fast and reliable booking';
    $lang['ban_3'] = 'Attractive confirmed price with no hidden costs';
    $lang['ban_4'] = 'Air-conditioned vehicles with modern equipment';
    $lang['ban_5'] = 'Payment by credit card or bank transfer';
    $lang['ban_6'] = 'Your professional driver will welcome you at the airport gate with your name written on a sign';
    $lang['vehicle'] = 'Vehicle';
    $lang['price_per_transfer'] = 'Price per transfer';
    $lang['per_transfer'] = 'per transfer';
    $lang['transfer_info'] = 'Transfer information';
    $lang['transfer_info_return'] = 'Return transfer information';
    $lang['flight_number'] = 'Flight number';
    $lang['arriving_from'] = 'Arriving from';
    $lang['airline_name'] = 'Airline name';
    $lang['from_hotel'] = 'From hotel';
    $lang['from_address'] = 'From address';
    $lang['to_hotel'] = 'To hotel';
    $lang['to_address'] = 'To address';
    $lang['hotel_or_property'] = 'Hotel or property';

    /* -----------------------------------------------------------------------
      RENTACAR
      ----------------------------------------------------------------------- */

    $lang['per_day'] = 'per day';
    $lang['rentacar.pickup'] = 'Pick up';
    $lang['rentacar.return'] = 'Return';
    $lang['rentacar.from'] = 'From';
    $lang['rentacar.to'] = 'To';
    $lang['rentacar.time'] = 'Time';
    $lang['rentacar.total'] = 'Total';
    $lang['rentacar.details'] = 'details';
    $lang['rentacar.score'] = 'Score';
    $lang['rentacar.technical.details'] = 'Technical details';
    $lang['rentacar.engine'] = 'Engine';
    $lang['rentacar.max.power'] = 'Max power';
    $lang['rentacar.max.speed'] = 'Maximal speed';
    $lang['rentacar.consumption'] = 'Consumption';
    $lang['rentacar.transmission'] = 'Transmission';
    $lang['rentacar.luggage.space.capacity'] = 'Luggage space capacity';
    $lang['rentacar.seat.count'] = 'Seat count';
    $lang['rentacar.number.of.doors'] = 'Number of doors';
    $lang['rentacar.automatic.transmission'] = 'Automatic transmission';
    $lang['rentacar.fuel'] = 'Fuel';
    $lang['rentacar.emmision'] = 'Emmision';
    $lang['rentacar.vehicle.equipment'] = 'Vehicle equipment';
    $lang['rentacar.day'] = 'day';
    $lang['rentacar.days'] = 'days';
    $lang['rentacar.seats'] = 'seats';
    $lang['rentacar.doors'] = 'doors';
    $lang['rentacar.ac'] = 'Air condition';
    $lang['rentacar.ab'] = 'Air bag';
    $lang['rentacar.bonus.equipment'] = 'Extra equipment';
    $lang['rentacar.add.bonus.equipment'] = 'Add extra equipment';
    $lang['rentacar.optional.accessories'] = 'Recommended extras';
    $lang['rentacar.set.selected.accessories'] = 'Select';
    $lang['rentacar.change.car'] = 'Change car';
    $lang['rentacar.locations'] = 'Locations';
    $lang['rentacar.remove'] = 'remove';
    $lang['rentacar.paid'] = 'Paid';
    $lang['rentacar.automatic'] = 'Automatic transmission';
    $lang['rentacar.manual'] = 'Manual transmission';
    $lang['not.availble.in.this.period'] = 'Car is not available in this period';

    /* -----------------------------------------------------------------------
      User reviews & scores
      ----------------------------------------------------------------------- */

    $lang['reviews.total_score'] = "Total Score";
    $lang['reviews.leave_comment'] = "Leave a Comment & Rate";
    $lang['reviews.name'] = "Name";
    $lang['reviews.surname'] = "Surname";
    $lang['reviews.score'] = "Score";
    $lang['reviews.comment'] = "Comment";
    $lang['reviews.submit'] = "Submit";
    $lang['reviews.comments'] = "Comments";

    $lang['5'] = 'Average';
    $lang['5.0'] = 'Average';
    $lang['5.1'] = 'Average';
    $lang['5.2'] = 'Average';
    $lang['5.3'] = 'Average';
    $lang['5.4'] = 'Average';
    $lang['5.5'] = 'Average';
    $lang['5.6'] = 'Average';
    $lang['5.7'] = 'Average';
    $lang['5.8'] = 'Average';
    $lang['5.9'] = 'Average';
    $lang['6'] = 'Good';
    $lang['6.0'] = 'Good';
    $lang['6.1'] = 'Good';
    $lang['6.2'] = 'Good';
    $lang['6.3'] = 'Good';
    $lang['6.4'] = 'Good';
    $lang['6.5'] = 'Good';
    $lang['6.6'] = 'Good';
    $lang['6.7'] = 'Good';
    $lang['6.8'] = 'Good';
    $lang['6.9'] = 'Good';
    $lang['7'] = 'Good';
    $lang['7.0'] = 'Good';
    $lang['7.1'] = 'Good';
    $lang['7.2'] = 'Good';
    $lang['7.3'] = 'Good';
    $lang['7.4'] = 'Good';
    $lang['7.5'] = 'Good';
    $lang['7.6'] = 'Good';
    $lang['7.7'] = 'Good';
    $lang['7.8'] = 'Good';
    $lang['7.9'] = 'Good';
    $lang['8'] = 'Very good';
    $lang['8.0'] = 'Very good';
    $lang['8.1'] = 'Very good';
    $lang['8.2'] = 'Very good';
    $lang['8.3'] = 'Very good';
    $lang['8.4'] = 'Very good';
    $lang['8.5'] = 'Excelent';
    $lang['8.6'] = 'Excelent';
    $lang['8.7'] = 'Excelent';
    $lang['8.8'] = 'Excelent';
    $lang['8.9'] = 'Excelent';
    $lang['9'] = 'Wonderful';
    $lang['9.0'] = 'Wonderful';
    $lang['9.1'] = 'Wonderful';
    $lang['9.2'] = 'Wonderful';
    $lang['9.3'] = 'Wonderful';
    $lang['9.4'] = 'Wonderful';
    $lang['9.5'] = 'Exceptional';
    $lang['9.6'] = 'Exceptional';
    $lang['9.7'] = 'Exceptional';
    $lang['9.8'] = 'Exceptional';
    $lang['9.9'] = 'Exceptional';
    $lang['10.0'] = 'Exceptional';
    