<?php

    if (!defined('BASEPATH')) exit('No direct script access allowed');

    $lang['info_reservation'] = "Reservation Info";

    $lang['customer_suport_department'] = "Customer Support Department";
    $lang['customer_suport_department_text'] = "Office hours: Mon - Fri: 09:00 - 17:00 h CET, Sat: 09:00 - 14:00 h CET, Infoline:";

    $lang['client_arrival_date'] = "Arrival date";
    $lang['client_departure_date'] = "Departure date";
    $lang['client_name'] = "Client's name";
    $lang['client_email'] = "Email";
    $lang['client_mobile_phone'] = "Mobile phone";
    $lang['client_address'] = "Address";
    $lang['client_zip'] = "ZIP Code / Country";
    $lang['client_nr_persons'] = "Number of persons";
    $lang['client_payment_method'] = "Payment method";
    $lang['client_payment_options'] = "Payment options";

    $lang['dear'] = "Dear";
    $lang['this_email'] = "This email is confirmation that we have recieved your reservation inquery.";

    $lang['our_service'] = "Our Customer Service will inform you about availability of ";
    $lang['within_24_h'] = "accommodation within 24 hours.";

    $lang['our_service_excursion'] = "Our Customer Service will inform you about availability of ";
    $lang['within_24_h_excursion'] = "excursion within 24 hours.";

    $lang['king_regards'] = "Kind Regards";

    $lang['cancelation'] = "Accommodation Cancellation";
    $lang['use_folowing_link'] = "To cancel your reservation please click on following link";

    $lang['pro_forma'] = "Reservation Info & Pro-forma invoice";

    $lang['client_arrival_departure'] = "Arrival / Departure";
    $lang['client_city'] = "City";


    $lang['accommodation_confirmation'] = "Accommodation Confirmation";
    $lang['inquiry_confirmation'] = "Inquiry Confirmation";
    $lang['destination'] = "Destination";
    $lang['accommodation'] = "Accommodation";

    $lang['total_price'] = "Total Price";
    $lang['room_service'] = "Room/Service";
    $lang['advanced_payment'] = "Advanced payment";
    $lang['upon_inquiry_30'] = "30% of the price of accommodation upon granted inquiry paid to the agency";
    $lang['ovreall_cost'] = "Ovreall cost of money transfer paymant paid by the customer";
    $lang['cost_by_the_client'] = "The payers and recipients bank transfer fees are covered entirely by the client";
    $lang['vat'] = "The Price includes VAT (19%)";
    $lang['tax'] = "Residential tax is included in accommodation price";
    $lang['additional_cost'] = "Please note: additional costs (eg extra bed) are not included in this purchase price";
    $lang['now_30_70_upon_arrival'] = "REST FOR PAYMENT: 70% balance upon arrival";
    $lang['now_30_70_upon_10_days_before_arrival'] = "REST FOR PAYMENT: 70% balance 10 days before arrival";
    $lang['total'] = "Total:";
    $lang['make_payment_within_72_hours'] = "Please make a payment within 48 hours under the selected payment method and dynamics. After your make a payment our customer service will send you VOUCHER and all information concerning your booking, such as the owner's name (contact person), accommodation address (or address at which you are applying on arrival) and phone number.";

    $lang['accepted_cards'] = "Accepted credit cards";

    $lang['thank_you_booking_with'] = "Thank you for booking with";
    $lang['we_hope'] = "We hope that you are pleased with our services and the accommodations that<br> we arranged for you.";
    $lang['we_hope_services'] = "We hope that you are pleased with our services.";
    $lang['nice_trip'] = "Have a nice trip";
    $lang['team'] = "Team";

    $lang['explanation_voucher'] = "Explanation on how <br/>to use the voucher:";
    $lang['provided_by'] = "This voucher is provided by ";
    $lang['present_passport'] = "Please present your passport when using the voucher.";

    $lang['30% now and 70% balance 10 days before arrival'] = "30% now and 70% balance 10 days before arrival";
    $lang['30% now and 70% balance upon arrival'] = "30% now and 70% balance upon arrival";
    $lang['Payment in full'] = "Payment in full";

    $lang['dtl.tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['dtl.tax_not_included'] = 'Residential tax and insurance are NOT included in the price and are being paid at check-in.';

    $lang["city_tax"] = "Residential tax";
    $lang["insurance_tax"] = "Insurance";
    $lang["eco_tax"] = "Eco tax";
    $lang["day"] = "day";
    $lang["note"] = "Note";
    $lang["per person/day"] = "per person/day";

    /* -----------------------------------------------------------------------
      Review
      ----------------------------------------------------------------------- */
    $lang['please_give_feedback'] = 'Please give us valuable feedback about accommodation that we arranged for you.';
    $lang['to_submit_review'] = 'To submit your review please click on following link';

    /* -----------------------------------------------------------------------
      Excursions
      ----------------------------------------------------------------------- */
    $lang['confirmation'] = "Confirmation";
    $lang['cancelation_clean'] = "Cancellation";
    $lang['cancelation_intro'] = "Information about cancellation should be sent by fax or e-mail, referring to the number of voucher";
    $lang['cancelation_fees'] = "Cancellation fees: 20% of a whole amount. ";
    $lang['excursion'] = "Excursion";
    $lang['start_date'] = "Start date";
    $lang['transport_type'] = "Transportation type";
    $lang['pickup_location'] = "Pickup location";
    $lang['add_on_during_journey'] = "Add on during journey";
    $lang['guides'] = 'Guides';
    $lang['adults'] = 'Adults';
    $lang['children'] = 'Children';
    $lang['week_days'] = array(
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    );
    $lang['transports'] = array(
        'Bus',
        'Boat',
        'Jeep',
        'Bus + Boat',
        'Bus + Jeep',
        'Bus + Canoe'
    );

    /* -----------------------------------------------------------------------
       Rent a car
       ----------------------------------------------------------------------- */
    $lang['pickup_date_time'] = 'Pickup date';
    $lang['return_date_time'] = 'Return date';
    $lang['persons'] = 'persons';
    $lang['add_on_during_booking'] = "Add on during booking";