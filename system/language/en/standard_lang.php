<?php

    $lang["montenegro"] = "Montenegro";

    $lang['about_us'] = 'About Us';
    $lang['customer_servcie'] = 'Customer Service';
    $lang['terms_conditions'] = 'Terms & Conditions';
    $lang['company_information'] = 'Company information';
    $lang['change_language'] = 'Change language';
    $lang['register_property'] = 'Register your property';

    $lang['we_recommend'] = 'We recommend';
    $lang['recently_booked'] = 'Recently booked';
    $lang['popular_destinations'] = 'Popular destinations';
    $lang['what_s_trending'] = 'What\'s trending';
    
    $lang['follow_us'] = 'Follow us';
    $lang['follow_us_on'] = 'Follow us on';
    
    $lang['all_rights_reserved'] = 'All rights reserved';

    