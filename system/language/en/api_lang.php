<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    // room types
    $lang['room.apartment'] = "Apartment";
    $lang['room.single_room'] = "Single room";
    $lang['room.double_room'] = "Double room";
    $lang['room.triple_room'] = "Triple room";
    $lang['room.four_bedded_room'] = "Four-bedded Room";
    $lang['room.four-bedded_room'] = "Four-bedded Room";
    $lang['room.family_room'] = "Family room";
    $lang['room.junior_suite'] = "Junior suite";
    $lang['room.bungalow'] = "Bungalow";
    $lang['room.room'] = "Room";
    $lang['room.twin_room'] = "Twin Room";
    $lang['room.studio'] = "Studio";

    // selling types
    $lang['room.selling_type'][1] = "one time fee";
    $lang['room.selling_type'][2] = "per person/night";
    $lang['room.selling_type'][5] = "per room/night";
    $lang['room.selling_type_short'][1] = "one time fee";
    $lang['room.selling_type_short'][2] = "person";
    $lang['room.selling_type_short'][5] = "per room";

    // object types
    $lang['type.apartment'] = 'apartment';
    $lang['type.hotel'] = 'hotel';
    $lang['type.villa'] = 'villa';
    $lang['type.authentic accommodation'] = 'authentic accommodation';
    $lang['type.youth hostel'] = 'youth hostel';
    $lang['type.houses with pool'] = 'houses with pool';
    $lang['type.rooms'] = 'rooms';


    // service type
    $lang["per person/night"] = "per person/night";
    $lang["per item per day"] = "per item per day";
    $lang["per room/night"] = "per room/night";

    // form
    $lang['form.acc_search'] = 'Search <strong>accommodation</strong>';
    $lang['form.destination_name'] = 'Destination name';
    $lang['form.checkin'] = 'Check-in Date';
    $lang['form.checkout'] = 'Check-out Date';
    $lang['form.specific'] = "I don't have specific dates yet";
    $lang['form.adults'] = "Adults";
    $lang['form.childrens'] = "Childrens";
    $lang['form.show_all_items'] = 'Show All Items';
    $lang['form.search'] = 'Search';

    $lang['form.slogan'] = 'Search for your perfect holiday';
    $lang['form.what'] = 'What';
    $lang['form.where'] = 'Where';
    $lang['form.when'] = 'When';
    $lang['form.person'] = 'Person';
    $lang['form.persons'] = 'Persons';
    $lang['form.persons_title'] = 'Persons';
    $lang['form.excl'] = 'excl. infant';
    $lang['form.private'] = 'Private accomm.';
    $lang['form.authentic'] = ' Authentic accomm.';
    $lang['form.dest_region'] = 'Destination or Region...';
    $lang['form.for'] = 'for';

    // filter
    $lang['filter.title'] = 'Filter your results';
    $lang['filter.price_by_night'] = 'Price (per Night)';
    $lang['filter.star_rating'] = "Star Rating";
    $lang['filter.star'] = "Star";
    $lang['filter.stars'] = "Stars";
    $lang['filter.unrated'] = "Unrated";
    $lang['filter.distances'] = "Distances";
    $lang['filter.no_preference'] = "No preference";
    $lang['filter.beach_within'] = "Beach within";
    $lang['filter.center_within'] = "Center within";
    $lang['filter.facilities_services'] = "Facilities & Services";
    $lang['filter.filtering'] = "Filtering";
    $lang['noresult'] = "No results";

    // results
    $lang['res.title_private'] = 'Private accommodations in Montenegro';
    $lang['res.title_hotel'] = 'Hotels in Montenegro';
    $lang['res.title_authentic'] = 'Authentic accommodation in Montenegro';
    $lang['res.title_hostel'] = 'Hostels in Montenegro';
    $lang['res.title_default'] = 'Accommodations in Montenegro';
    $lang['res.accommodation'] = 'Accommodation';
    $lang['res.service'] = 'Service';
    $lang['res.total_price'] = 'Total Price';
    $lang['price'] = 'Price';
    $lang['res.booking'] = 'reserve';
    $lang['res.night'] = 'night';
    $lang['res.nights'] = 'nights';
    $lang['res.person'] = 'person';
    $lang['res.persons'] = 'persons';
    $lang['res.book_now'] = 'Book now';
    $lang['res.send_request'] = 'Send request';
    $lang['res.price_for'] = 'price for';
    $lang['hotell.sort_by_upper'] = "Sort By";

    // details
    $lang['dtl.see_all_reviews'] = 'See all reviews';
    $lang['dtl.reviews'] = 'Guest reviews';
    $lang['dtl.rating'] = 'Guest Rating';
    $lang['dtl.based_on'] = 'Based on ';
    $lang['dtl.guest_reviews'] = ' guest reviews ';
    $lang['dtl.room'] = 'Room';
    $lang['dtl.check_in'] = 'Check in';
    $lang['dtl.check_out'] = 'Check out';
    $lang['dtl.accepted_credit_cards'] = 'Accepted credit cards';
    $lang['dtl.pets'] = 'Pets';
    $lang['dtl.num_of_persons'] = 'Number of persons';
    $lang['dtl.adults'] = 'adults';
    $lang['dtl.teenagers'] = 'teenagers';
    $lang['dtl.children'] = 'children';
    $lang['dtl.babies'] = 'babies';
    $lang['dtl.period'] = 'Period';
    $lang['dtl.min_stay'] = 'Minimum stay';
    $lang['dtl.information'] = 'Information';
    $lang['dtl.prices_availability'] = 'Prices & Availability';
    $lang['dtl.map_distances'] = 'Map & Distances';
    $lang['dtl.distances_from'] = 'Distances from';
    $lang['dtl.cancellation_policy'] = 'Cancellation Policy';
    $lang['dtl.children_and_extra_beds'] = 'Children and extra beds';
    $lang['dtl.payment_conditions'] = 'Payment conditions';
    $lang['dtl.payment_conditions_text'] = 'Choose valid period to see payment methods and dynamics for this accommodation.';
    $lang['dtl.question'] = 'Got a question';
    $lang['dtl.question_text'] = 'Got a question about this accommodation before you book?<br>Feel free to post your question here and we will respond as soon as possible.';
    $lang['dtl.posting_question'] = 'Posting your question';
    $lang['dtl.posting_thanks'] = 'Thank you for your question';
    $lang['dtl.subscribing'] = 'Subscribing your email';
    $lang['dtl.subscribe_thanks'] = 'Thank you for subscribing to our newsletter.';
    $lang['dtl.subscribe_alredy'] = 'You already subscribed to our newsletter.';
    $lang['dtl.tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['name'] = 'Name';
    $lang['surname'] = 'Surname';
    $lang['country'] = 'Country';
    $lang['email'] = 'E-mail';
    $lang['mobile'] = 'Mobile phone';
    $lang['newsletter'] = 'By filling out this form, I agree to receive newsletters from bookme-montenegro.com and accept their General Terms and conditions';
    $lang['note'] = 'Note';
    $lang['send'] = 'Send';
    $lang['description'] = 'Description';
    $lang['one_time_registration_is'] = 'One time registration is';
    $lang['other_accommodation_units'] = 'Other accommodation units';
    $lang['basic_service'] = 'Select the basic service';
    $lang['define_date'] = 'Define the date of your stay';
    $lang['define_arrival'] = 'Date of arrival';
    $lang['define_departure'] = 'Date of departure';
    $lang['no_persons'] = 'Please enter the number of persons';
    $lang['available'] = 'Available';
    $lang['occupied'] = 'Occupied';
    $lang['selected'] = 'Selected';
    $lang['arrival'] = 'arrival';
    $lang['departure'] = 'departure';
    $lang['instructions'] = 'Instructions: With the 1st click on the field marked date select the beginning date of your vacation. With the 2nd click select the end date of your vacation.';
    $lang['cal_info'] = "The calendar is for informational purposes only, send a request to agency.";
    $lang['calculation'] = 'Calculation';
    $lang['total'] = 'Total';
    $lang['calculating'] = 'Calculating';
    $lang['hotel.facilities'] = "Facilities";
    $lang['hotel.polices'] = "Polices";
    $lang['read_more'] = "Read more";
    $lang['see_more'] = "See more";
    $lang['close'] = "Close";
    $lang['dtl.tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['dtl.tax_not_included'] = 'Residential tax and insurance are NOT included in the price and are being paid at check-in.';
    $lang["city_tax"] = "Residential tax";
    $lang["insurance_tax"] = "Insurance";
    $lang["eco_tax"] = "Eco tax";

    // not available messages
    $lang['not_available'] = 'Room is not availble in this period!';
    $lang['too_meny_person'] = 'You have selected too many persons!';
    $lang['min_stay'] = 'Minimum stay';
    $lang['min_ps'] = 'Minimum persons is ';
    $lang['days'] = 'nights';
    $lang['person'] = 'person';

    // checkout
    $lang['c.date_selection'] = 'Date Selection';
    $lang['c.reservation_details'] = 'Reservation Details';
    $lang['c.confirmation'] = 'Confirmation';
    $lang['c.location'] = 'Location';
    $lang['c.type_accomm'] = 'Type of accommmodation';
    $lang['c.change_settings'] = 'Change settings';
    $lang['c.inquery_info'] = 'Enter your personal details in the reservation form.<br>Our customer service will send you an e-mail about the availability within 12 hours (24 hours on weekends and holidays). <br> In case of availability we will inform you by e-mail how to proceed with the reservation process and continue with the selection of payment method.';
    $lang['c.info_reservation_holder'] = 'Information on the reservation holder';
    $lang['gender'] = 'Gender';
    $lang['male'] = 'Male';
    $lang['female'] = 'Female';
    $lang['date_of_birth'] = 'Date of birth';
    $lang['mobile_phone'] = 'Mobile phone';
    $lang['city'] = 'City';
    $lang['address'] = 'Address';
    $lang['zip_code'] = 'ZIP Code';
    $lang['c.methods_of_payment'] = 'Methods of payment';
    $lang['c.cc_online'] = 'Credit card online';
    $lang['c.cc_offline'] = 'Credit card using offline authorization';
    $lang['c.bank_trasfer'] = 'Bank transfer in EUR';
    $lang['c.wu'] = 'Western Union';
    $lang['c.payment_in_full'] = 'Payment in full';
    $lang['c.30_70'] = '30% now and 70% balance upon arrival';
    $lang['c.30_70_10'] = '30% now and 70% balance 10 days before arrival';
    $lang['c.i_have_read'] = 'I have read and accept the';
    $lang['c.general_terms'] = 'general terms';
    $lang['c.conditions'] = 'conditions';
    $lang['make_reservation'] = 'Make Reservation';
    $lang['make_request'] = 'Make Request';
    $lang['advance_payment'] = 'Advance payment';
    $lang['guest_and_reservation_data'] = 'Guest and reservation data';
    $lang['price_calculation'] = 'Price calculation';
    $lang['selected_facility'] = 'Selected facility';
    $lang['unit'] = 'Unit';
    $lang['fill_out_form'] = 'Please fill out the form to get an accurate price calculation';
    $lang['amount'] = 'Amount';
    $lang['no_nights'] = 'Number of nights';
    $lang['third_party'] = 'Bookme-montenegro.com agrees that the information entered on this site are used solely for the purpose of providing the
            requested service. Your personal information will be forwarded to a third party or service provider, for the
            purpose of recording reservations on your behalf. If it comes to your personal information changes or if
            there is an error when you type them, please notify us.';
    $lang['reservations'] = 'Reservations';
    $lang['directly'] = 'Call us directly';


    $lang['thanx_inquery_sent'] = 'Inquiry sent';
    $lang['thanx_text'] = 'Thank you for your inquiry.';

    $lang['thanx_realtime_sent'] = 'Reservation Confirmed';
    $lang['thanx_realtime_text'] = 'Thank you for your reservation.';

    $lang['thanx_payment_text'] = 'Thank you for your reservation.<br><br>For any additional questions or information, please contact our customer service.<br><br>Thank you for booking with <b>bookme-montenegro.com</b>';


    // excursions
    $lang['exc.excursions'] = 'Excursions';
    $lang['exc.excursion'] = 'Excursion';
    $lang['exc.discover'] = 'Discover + Enjoy';
    $lang['exc_categories'] = 'Categories';
    $lang['exc_category'] = 'Category';
    $lang['exc_intro'] = 'Looking for adventure, natural beauty and a unique itinerary in <strong>Montenegro</strong>, contact <strong>cipa-booking.com</strong>. We offer you a large selection of excursions carefully designed to meet all your expectations and bring your travel fantasies to life. Our most captivating excursions itineraries will take you to the places you never dreamed of visiting. Let us show you <strong>Montenegro</strong> in a new and adventurous way.';
    $lang['per_person'] = 'per person';
    $lang['start_day'] = 'Departure day';
    $lang['guides'] = 'Guides';
    $lang['transportation_type'] = 'Transportation type';
    $lang['addition'] = 'Addition';
    $lang['pickup_location'] = 'Pickup Location';
    $lang['description'] = 'Description';
    $lang['exc_note'] = '<p>Prices are valid for summer season.</p><p>In the off season please contact agency for prices details.</p>';
    $lang['exc_similar'] = 'Similar excursions';
    $lang['exc_cal_instructions'] = 'Instructions: Click on  calendar to select the date of departure.';
    $lang['exc_departure_day'] = 'Please choose departure day';
    $lang['exc_service'] = 'Service';
    $lang['please_select'] = '-- Please select';
    $lang['week_days'] = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $lang['transports'] = array('Bus', 'Boat', 'Jeep', 'Bus + Boat', 'Bus + Jeep', 'Bus + Canoe');
    $lang['places_left'] = 'The remaining places ';
    $lang['exc_autocomp'] = 'History & Culture, Nature, Sport & Adventure';
    $lang['sellingSeason'] = 'Selling season: ';

    // transfers
    $lang['transfer'] = 'Transfer';
    $lang['find_transfer'] = 'Find transfer';
    $lang['date'] = 'Date';
    $lang['date_return'] = 'Return date';
    $lang['time'] = 'Time';
    $lang['book_a_return'] = 'Book a return';
    $lang['ban_intro'] = 'Book your transfer on time, in just a few minutes, with a reliable partner';
    $lang['ban_1'] = 'No waiting in line for a taxi';
    $lang['ban_2'] = 'Simple, fast and reliable booking';
    $lang['ban_3'] = 'Attractive confirmed price with no hidden costs';
    $lang['ban_4'] = 'Air-conditioned vehicles with modern equipment';
    $lang['ban_5'] = 'Payment by credit card or bank transfer';
    $lang['ban_6'] = 'Your professional driver will welcome you at the airport gate with your name written on a sign';
    $lang['vehicle'] = 'Vehicle';
    $lang['price_per_transfer'] = 'Price per transfer';
    $lang['per_transfer'] = 'per transfer';
    $lang['transfer_info'] = 'Transfer information';
    $lang['transfer_info_return'] = 'Return transfer information';
    $lang['flight_number'] = 'Flight number';
    $lang['arriving_from'] = 'Arriving from';
    $lang['airline_name'] = 'Airline name';
    $lang['from_hotel'] = 'From hotel';
    $lang['from_address'] = 'From address';
    $lang['to_hotel'] = 'To hotel';
    $lang['to_address'] = 'To address';
    $lang['hotel_or_property'] = 'Hotel or property';

    // rent a car
    $lang['per_day'] = 'per day';

    /* RENTACAR */
    $lang['rentacar.pickup'] = 'Pick up';
    $lang['rentacar.return'] = 'Return';
    $lang['rentacar.from'] = 'From';
    $lang['rentacar.to'] = 'To';
    $lang['rentacar.time'] = 'Time';
    $lang['rentacar.total'] = 'Total';
    $lang['rentacar.details'] = 'details';
    $lang['rentacar.score'] = 'Score';
    $lang['rentacar.technical.details'] = 'Technical details';
    $lang['rentacar.engine'] = 'Engine';
    $lang['rentacar.max.power'] = 'Max power';
    $lang['rentacar.max.speed'] = 'Maximal speed';
    $lang['rentacar.consumption'] = 'Consumption';
    $lang['rentacar.transmission'] = 'Transmission';
    $lang['rentacar.luggage.space.capacity'] = 'Luggage space capacity';
    $lang['rentacar.seat.count'] = 'Seat count';
    $lang['rentacar.number.of.doors'] = 'Number of doors';
    $lang['rentacar.automatic.transmission'] = 'Automatic transmission';
    $lang['rentacar.fuel'] = 'Fuel';
    $lang['rentacar.emmision'] = 'Emmision';
    $lang['rentacar.vehicle.equipment'] = 'Vehicle equipment';
    $lang['rentacar.day'] = 'day';
    $lang['rentacar.days'] = 'days';
    $lang['rentacar.seats'] = 'seats';
    $lang['rentacar.doors'] = 'doors';
    $lang['rentacar.ac'] = 'Air condition';
    $lang['rentacar.ab'] = 'Air bag';
    $lang['rentacar.bonus.equipment'] = 'Extra equipment';
    $lang['rentacar.add.bonus.equipment'] = 'Add extra equipment';
    $lang['rentacar.optional.accessories'] = 'Recommended extras';
    $lang['rentacar.set.selected.accessories'] = 'Select';
    $lang['rentacar.change.car'] = 'Change car';
    $lang['rentacar.locations'] = 'Locations';
    $lang['rentacar.remove'] = 'remove';
    $lang['rentacar.paid'] = 'Paid';
    $lang['rentacar.automatic'] = 'Automatic transmission';
    $lang['rentacar.manual'] = 'Manual transmission';
    $lang['not.availble.in.this.period'] = 'Car is not available in this period';

    // reviews
    $lang['reviews.total_score'] = "Total Score";
    $lang['reviews.leave_comment'] = "Leave a Comment & Rate";
    $lang['reviews.name'] = "Name";
    $lang['reviews.surname'] = "Surname";
    $lang['reviews.score'] = "Score";
    $lang['reviews.comment'] = "Comment";
    $lang['reviews.submit'] = "Submit";
    $lang['reviews.comments'] = "Comments";

    // currencies
    $lang['EUR'] = '&euro;';
    $lang['RUB'] = ' RUB ';
    $lang['USD'] = '$';

    $lang["pricelist"] = "Price list";
    $lang["mindays"] = "Min. Days";
    $lang["minpersons"] = "Min. Persons";
    $lang["day"] = "day";
    $lang['in'] = 'in';
    $lang['listing'] = 'Showing';
    $lang['more_rooms'] = 'Show more rooms';
    $lang['hide_rooms'] = 'Hide rooms';
    $lang['important'] = 'Important information';
    $lang['personal_info'] = 'Personal information';
    $lang['contact_info'] = 'Contact information';
    $lang['spec_request'] = 'Special request';
    $lang['spec_req_text'] = 'Do you have any special requests for your stay? Your request will be received when you completed booking.';
    $lang['res_info'] = 'Reservation details';
    $lang['show_accomm'] = 'Show accommodation in';
    $lang["price_in"] = "Price in";
    $lang['short'] = 'e.g.';
    $lang['vat'] = 'The Price includes VAT (19%)';
    $lang['from'] = 'from';
    $lang['recommended'] = 'Recommended';
    $lang['view_details'] = 'view details';
    $lang['click_for_more_info'] = 'availability info & prices';

    /* HOTEL SCORE */
    $lang['5'] = 'Average';
    $lang['5.0'] = 'Average';
    $lang['5.1'] = 'Average';
    $lang['5.2'] = 'Average';
    $lang['5.3'] = 'Average';
    $lang['5.4'] = 'Average';
    $lang['5.5'] = 'Average';
    $lang['5.6'] = 'Average';
    $lang['5.7'] = 'Average';
    $lang['5.8'] = 'Average';
    $lang['5.9'] = 'Average';
    $lang['6'] = 'Good';
    $lang['6.0'] = 'Good';
    $lang['6.1'] = 'Good';
    $lang['6.2'] = 'Good';
    $lang['6.3'] = 'Good';
    $lang['6.4'] = 'Good';
    $lang['6.5'] = 'Good';
    $lang['6.6'] = 'Good';
    $lang['6.7'] = 'Good';
    $lang['6.8'] = 'Good';
    $lang['6.9'] = 'Good';
    $lang['7'] = 'Good';
    $lang['7.0'] = 'Good';
    $lang['7.1'] = 'Good';
    $lang['7.2'] = 'Good';
    $lang['7.3'] = 'Good';
    $lang['7.4'] = 'Good';
    $lang['7.5'] = 'Good';
    $lang['7.6'] = 'Good';
    $lang['7.7'] = 'Good';
    $lang['7.8'] = 'Good';
    $lang['7.9'] = 'Good';
    $lang['8'] = 'Very good';
    $lang['8.0'] = 'Very good';
    $lang['8.1'] = 'Very good';
    $lang['8.2'] = 'Very good';
    $lang['8.3'] = 'Very good';
    $lang['8.4'] = 'Very good';
    $lang['8.5'] = 'Excelent';
    $lang['8.6'] = 'Excelent';
    $lang['8.7'] = 'Excelent';
    $lang['8.8'] = 'Excelent';
    $lang['8.9'] = 'Excelent';
    $lang['9'] = 'Wonderful';
    $lang['9.0'] = 'Wonderful';
    $lang['9.1'] = 'Wonderful';
    $lang['9.2'] = 'Wonderful';
    $lang['9.3'] = 'Wonderful';
    $lang['9.4'] = 'Wonderful';
    $lang['9.5'] = 'Exceptional';
    $lang['9.6'] = 'Exceptional';
    $lang['9.7'] = 'Exceptional';
    $lang['9.8'] = 'Exceptional';
    $lang['9.9'] = 'Exceptional';
    $lang['10.0'] = 'Exceptional';
    