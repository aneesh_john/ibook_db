<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    $lang['home-title'] = 'Welcome | bookme-montenegro.com';
    $lang['home-description'] = 'Welcome to bookme-montenegro.com.  We are offering widest range of hotels and private accommodations in Montenegro. No additional charges when booking.';
    $lang['home-keywords'] = 'montenegro, booking, hotels, private accommodations, appartments, online booking, budva, tivat, kotor, herceg novi, ulcinj, kolasin, zabljak';

    $lang['search-title'] = 'Accommodations in Montenegro | Apartments and hotels | bookme-montenegro.com';
    $lang['search-description'] = 'Accommodations in Montenegro. A complete offer with all accommodations in Montenegro. Book an accommodation in a hotel room or apartment for your holiday in Montenegro';

    $lang['reservation-title'] = 'Reservation details | bookme-montenegro.com';
    $lang['thanx-title'] = 'Confirmation | bookme-montenegro.com';

    $lang['title-transfers'] = 'Transfers in Montenegro';
    $lang['description-transfers'] = 'We currently provide car transfers for airports and cities in Montenegro';

    // room types
    $lang['room.apartment'] = "Apartment";
    $lang['room.single_room'] = "Single room";
    $lang['room.double_room'] = "Double room";
    $lang['room.triple_room'] = "Triple room";
    $lang['room.four_bedded_room'] = "Four-bedded Room";
    $lang['room.family_room'] = "Family room";
    $lang['room.junior_suite'] = "Junior suite";
    $lang['room.bungalow'] = "Bungalow";
    $lang['room.room'] = "Room";
    $lang['room.twin_room'] = "Twin Room";
    $lang['room.studio'] = "Studio";

    // selling types
    $lang['room.selling_type'][1] = "one time fee";
    $lang['room.selling_type'][2] = "per person/night";
    $lang['room.selling_type'][5] = "per room/night";
    $lang['room.selling_type_short'][1] = "one time fee";
    $lang['room.selling_type_short'][2] = "person";
    $lang['room.selling_type_short'][5] = "per room";

    // object types
    $lang['type.apartment'] = 'apartment';
    $lang['type.hotel'] = 'hotel';
    $lang['type.villa'] = 'villa';
    $lang['type.authentic accommodation'] = 'authentic accommodation';
    $lang['type.youth hostel'] = 'youth hostel';

    // service type
    $lang["per person/night"] = "per person/night";
    $lang["per item per day"] = "per item per day";
    $lang["per room/night"] = "per room/night";

    $lang["price_in"] = "Price in";

    $lang['slogan'] = 'Montenegro, your next destination';
    $lang['slogan_sub'] = 'Book over 1,000 hotels and apartments';
    $lang['slogan_form'] = '1,000 hotels and apartments';
    $lang['short'] = 'e.g.';

    $lang['why_1'] = 'Own hotel or apartment';
    $lang['why_1_sub'] = 'Join largest accommodation selection in Montenegro';
    $lang['why_2'] = 'Over 1, 000 accommodations';
    $lang['why_2_sub'] = 'Book over 1,000 accommodations without booking charges';

    $lang['guide_1'] = 'Not sure where to stay in Montenegro';
    $lang['guide_2'] = 'Explore our top destinations guide.';
    $lang['guide_3'] = 'We\'ve created top destinations guide to help you choose the right Montenegro hotel or apartment.';

    $lang['foot_about'] = 'About';
    $lang['rights_reserved'] = 'All rights reserved';
    $lang['dev_by'] = 'Developed by';

    $lang['show_map_1'] = 'Browse largest accommodation selection in Montenegro through interactive accommodation map. For easier navigation, accommodations are grouped into regions with count of accommodation in each region.';
    $lang['show_map_2'] = 'Click on region to show all accommodation capacities with information about prices and reviews. By clicking on accommodation link, you are automatically redirected to accommodation details and reservation page.';




    /*
     * LOGO
     */
    $lang['montenegro'] = 'Montenegro';

    /*
     * TOP NAV
     */
    $lang['top.about'] = 'About Us';
    $lang['top.servcie'] = 'Customer Service';
    $lang['top.language'] = 'Language';
    $lang['top.currency'] = 'Currency';
    $lang['top.custom_search'] = 'Destination or specific accommodation...';

    /*
     * NAV
     */
    $lang['nav.private'] = 'Private<br>accommodation';
    $lang['nav.holiday_homes'] = 'Holiday homes';
    $lang['nav.hotels'] = 'Hotels';
    $lang['nav.hostels_sub'] = 'Youth Hostels';
    $lang['nav.transfers'] = 'Transfers';
    $lang['nav.excursions'] = 'Excursions';
    $lang['nav.authentic'] = 'Authentic';
    $lang['nav.youth'] = 'Hostels';
    $lang['nav.rentacar'] = 'Rent a Car';
    $lang['nav.accommodation.private'] = 'Accommodation';
    $lang['nav.other_services'] = 'Other services';
    $lang['nav.guide'] = 'Tourist Guide';
    $lang['nav.cruises'] = 'Cruises';
    $lang['nav.special_offers'] = 'Special <br> Offers';

    /*
     * FORM
     */
    $lang['form.acc_search'] = 'Search <strong>accommodation</strong>';
    $lang['form.destination_name'] = 'Destination name';
    $lang['form.checkin'] = 'Check-in Date';
    $lang['form.checkout'] = 'Check-out Date';
    $lang['form.specific'] = "I don't have specific dates yet";
    $lang['form.adults'] = "Adults";
    $lang['form.childrens'] = "Childrens";
    $lang['form.show_all_items'] = 'Show All Items';
    $lang['form.search'] = 'Search';

    $lang['form.slogan'] = 'Search for your perfect holiday';
    $lang['form.what'] = 'What';
    $lang['form.where'] = 'Where';
    $lang['form.when'] = 'When';
    $lang['form.person'] = 'Person';
    $lang['form.persons'] = 'Persons';
    $lang['form.persons_title'] = 'Persons';
    $lang['form.excl'] = 'excl. infant';
    $lang['form.private'] = 'Private accomm.';
    $lang['form.authentic'] = ' Authentic accomm.';
    $lang['form.dest_region'] = 'Destination or Region...';
    $lang['form.for'] = 'for';


    /*
     * Banner Recommended
     */
    $lang['from'] = 'from';
    $lang['recommended'] = 'Recommended';
    $lang['view_details'] = 'view details';
    $lang['click_for_more_info'] = 'availability info & prices';


    /*
     * Customer Service
     */
    $lang['cust.cust'] = 'Customer service';
    $lang['cust.hours'] = 'Mon - Sat: 08:00 - 20:00 h';
    $lang['cust.email'] = 'Send us an e-mail:';
    $lang['cust.exposal'] = 'We are at your disposal';
    $lang['cust.follow'] = 'Follow us on';
    $lang['cust.exposal2'] = 'At your disposal';
    $lang['cust.provider'] = 'Local provider of tourist services';
    $lang['find_us_easy'] = 'Find us easy';
    $lang['cust_belgrade'] = 'Belgrade';
    $lang['cust_serbia'] = 'Serbia';
    $lang['montenegro'] = 'Montenegro';

    /*
     * Speacial Offer
     */
    $lang['t.special_offer'] = 'Special offer';
    $lang['t.popular'] = 'Popular on bookme-montenegro.com';
    $lang['t.top_destinations'] = 'Top destinations';
    $lang['t.top_accommodations'] = 'Top accommodation';
    $lang['t.doo_and_see'] = 'Things to do and see in Montenegro';
    $lang['top_beaches'] = 'Top beaches';
    $lang['national_parks'] = 'National Parks';
    $lang['canyons'] = 'Canyons';
    $lang['t.make_inquiry'] = 'Make Inquiry';

    /*
     * Show on map, Recently booked & Top Destinations
     */
    $lang['map.show_on_map'] = 'Show on map';
    $lang['appartments'] = 'Apartments';
    $lang['recently_booked'] = 'Recently Booked';
    $lang['top_testinations'] = 'Top Destinations';
    $lang['accommodations'] = 'Accommodations';
    $lang['map_private'] = '3000 private accommodation units';
    $lang['map_hotels'] = 'more than 150 hotels';
    $lang['map_hostels'] = 'more than 20 hostels';
    $lang['map_slogan'] = 'See complete offer and select the accommodation that suits you best!';

    /*
     * Mailbox
     */
    $lang['mail_slogan'] = 'Subscribe to our Newsletter!';
    $lang['mail_email'] = 'Enter Your email address';
    $lang['subscripbe'] = 'Subscribe';
    $lang['mail_info_desc'] = 'WE DELIVER OUR BEST DEALS<br>TO YOUR INBOX.<br>SUBSCRIBE TO OUR<br>NEWSLETTER!';

    /*
     * Footer
     */
    $lang['foot.title'] = 'Company Info';
    $lang['foot.how_book'] = 'How to book?';
    $lang['foot.contact'] = 'Contact';
    $lang['foot.terms'] = 'Terms and conditions';
    $lang['foot.useful_links'] = 'Useful links';
    $lang['foot.links'] = 'Links';
    $lang['foot.make_res'] = 'How to make a reservation';
    $lang['foot.payment'] = 'Payment options';
    $lang['foot.terms'] = 'Terms & Conditions';
    $lang['foot.owners'] = 'Cooperation with the owners';
    $lang['foot.links'] = 'Helpful Links';
    $lang['foot.job'] = 'Jobs';

    /*
     * Best prices
     */
    $lang['best.title'] = 'Why use bookme-montenegro.com?';
    $lang['best.best'] = 'Book & Save up to 100€';
    $lang['best.charges'] = 'No booking fees';
    $lang['best.experts'] = 'Money back guarantee';
    $lang['best.clients'] = 'Thousands of satisfied clients';
    $lang['best.experiance'] = 'Pay upon arrival';

    /*
     * Book safely
     */
    $lang['safley.title'] = 'Book safely with bookme-montenegro.com';
    $lang['safley.text'] = 'We use the best protocols (SSL) and standards (3D secure) for the security of Your online payments over the Internet.';

    /*
     * Filter
     */
    $lang['filter.title'] = 'Filter your results';
    $lang['filter.price_by_night'] = 'Price (per Night)';
    $lang['filter.star_rating'] = "Star Rating";
    $lang['filter.star'] = "Star";
    $lang['filter.stars'] = "Stars";
    $lang['filter.unrated'] = "Unrated";
    $lang['filter.distances'] = "Distances";
    $lang['filter.no_preference'] = "No preference";
    $lang['filter.beach_within'] = "Beach within";
    $lang['filter.center_within'] = "Center within";
    $lang['filter.facilities_services'] = "Facilities & Services";
    $lang['filter.filtering'] = "Filtering";
    $lang['noresult'] = "No results";

    /*
     * Results
     */

    $lang['res.title_private'] = 'Private accommodations in Montenegro';
    $lang['res.title_hotel'] = 'Hotels in Montenegro';
    $lang['res.title_authentic'] = 'Authentic accommodation in Montenegro';
    $lang['res.title_hostel'] = 'Hostels in Montenegro';
    $lang['res.title_default'] = 'Accommodations in Montenegro';
    $lang['res.accommodation'] = 'Accommodation';
    $lang['res.service'] = 'Service';
    $lang['res.total_price'] = 'Total Price';
    $lang['price'] = 'Price';
    $lang['res.booking'] = 'reserve';
    $lang['res.night'] = 'night';
    $lang['res.nights'] = 'nights';
    $lang['res.person'] = 'person';
    $lang['res.persons'] = 'persons';
    $lang['res.book_now'] = 'Book now';
    $lang['res.send_request'] = 'Send request';
    $lang['res.price_for'] = 'price for';
    $lang['hotell.sort_by_upper'] = "Sort By";

    /*
     * Details
     */

    $lang['dtl.see_all_reviews'] = 'See all reviews';
    $lang['dtl.reviews'] = 'Guest reviews';
    $lang['dtl.rating'] = 'Guest Rating';
    $lang['dtl.based_on'] = 'Based on ';
    $lang['dtl.guest_reviews'] = ' guest reviews ';
    $lang['dtl.room'] = 'Room';
    $lang['dtl.check_in'] = 'Check in';
    $lang['dtl.check_out'] = 'Check out';
    $lang['dtl.accepted_credit_cards'] = 'Accepted credit cards';
    $lang['dtl.pets'] = 'Pets';
    $lang['dtl.num_of_persons'] = 'Number of persons';
    $lang['dtl.adults'] = 'adults';
    $lang['dtl.teenagers'] = 'teenagers';
    $lang['dtl.children'] = 'children';
    $lang['dtl.babies'] = 'babies';
    $lang['dtl.period'] = 'Period';
    $lang['dtl.min_stay'] = 'Minimum stay';
    $lang['dtl.information'] = 'Information';
    $lang['dtl.prices_availability'] = 'Prices & Availability';
    $lang['dtl.map_distances'] = 'Map & Distances';
    $lang['dtl.distances_from'] = 'Distances from';
    $lang['dtl.cancellation_policy'] = 'Cancellation Policy';
    $lang['dtl.children_and_extra_beds'] = 'Children and extra beds';
    $lang['dtl.payment_conditions'] = 'Payment conditions';
    $lang['dtl.payment_conditions_text'] = 'Choose valid period to see payment methods and dynamics for this accommodation.';
    $lang['dtl.question'] = 'Got a question';
    $lang['dtl.question_text'] = 'Got a question about this accommodation before you book?<br>Feel free to post your question here and we will respond as soon as possible.';
    $lang['dtl.posting_question'] = 'Posting your question';
    $lang['dtl.posting_thanks'] = 'Thank you for your question';
    $lang['dtl.subscribing'] = 'Subscribing your email';
    $lang['dtl.subscribe_thanks'] = 'Thank you for subscribing to our newsletter.';
    $lang['dtl.subscribe_alredy'] = 'You already subscribed to our newsletter.';
    $lang['dtl.tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['name'] = 'Name';
    $lang['surname'] = 'Surname';
    $lang['country'] = 'Country';
    $lang['email'] = 'E-mail';
    $lang['mobile'] = 'Mobile phone';
    $lang['newsletter'] = 'By filling out this form, I agree to receive newsletters from bookme-montenegro.com and accept their General Terms and conditions';
    $lang['note'] = 'Note';
    $lang['send'] = 'Send';
    $lang['description'] = 'Description';
    $lang['one_time_registration_is'] = 'One time registration is';
    $lang['other_accommodation_units'] = 'Other accommodation units';
    $lang['basic_service'] = 'Select the basic service';
    $lang['define_date'] = 'Define the date of your stay';
    $lang['define_arrival'] = 'Date of arrival';
    $lang['define_departure'] = 'Date of departure';
    $lang['no_persons'] = 'Please enter the number of persons';
    $lang['available'] = 'Available';
    $lang['occupied'] = 'Occupied';
    $lang['selected'] = 'Selected';
    $lang['arrival'] = 'arrival';
    $lang['departure'] = 'departure';
    $lang['instructions'] = 'Instructions: With the 1st click on the field marked date select the beginning date of your vacation. With the 2nd click select the end date of your vacation.';
    $lang['cal_info'] = "The calendar is for informational purposes only, send a request to agency.";
    $lang['calculation'] = 'Calculation';
    $lang['total'] = 'Total';
    $lang['calculating'] = 'Calculating';
    $lang['hotel.facilities'] = "Facilities";
    $lang['hotel.polices'] = "Polices";
    $lang['read_more'] = "Read more";
    $lang['see_more'] = "See more";
    $lang['close'] = "Close";

    /*
     * Not Available Service Msg
     */

    $lang['not_available'] = 'Room is not availble in this period!';
    $lang['too_meny_person'] = 'You have selected too many persons!';
    $lang['min_stay'] = 'Minimum stay';
    $lang['min_ps'] = 'Minimum persons is ';
    $lang['days'] = 'nights';
    $lang['person'] = 'person';

    /*
     * Checkout
     */

    $lang['c.date_selection'] = 'Date Selection';
    $lang['c.reservation_details'] = 'Reservation Details';
    $lang['c.confirmation'] = 'Confirmation';
    $lang['c.location'] = 'Location';
    $lang['c.type_accomm'] = 'Type of accommmodation';
    $lang['c.change_settings'] = 'Change settings';
    $lang['c.inquery_info'] = 'Enter your personal details in the reservation form.<br>Our customer service will send you an e-mail about the availability within 12 hours (24 hours on weekends and holidays). <br> In case of availability we will inform you by e-mail how to proceed with the reservation process and continue with the selection of payment method.';
    $lang['c.info_reservation_holder'] = 'Information on the reservation holder';
    $lang['gender'] = 'Gender';
    $lang['male'] = 'Male';
    $lang['female'] = 'Female';
    $lang['date_of_birth'] = 'Date of birth';
    $lang['mobile_phone'] = 'Mobile phone';
    $lang['city'] = 'City';
    $lang['address'] = 'Address';
    $lang['zip_code'] = 'ZIP Code';
    $lang['c.methods_of_payment'] = 'Methods of payment';
    $lang['c.cc_online'] = 'Credit card online';
    $lang['c.cc_offline'] = 'Credit card using offline authorization';
    $lang['c.bank_trasfer'] = 'Bank transfer in EUR';
    $lang['c.wu'] = 'Western Union';
    $lang['c.payment_in_full'] = 'Payment in full';
    $lang['c.30_70'] = '30% now and 70% balance upon arrival';
    $lang['c.30_70_10'] = '30% now and 70% balance 10 days before arrival';
    $lang['c.i_have_read'] = 'I have read and accept the';
    $lang['c.general_terms'] = 'general terms';
    $lang['c.conditions'] = 'conditions';
    $lang['make_reservation'] = 'Make Reservation';
    $lang['make_request'] = 'Make Request';
    $lang['advance_payment'] = 'Advance payment';
    $lang['guest_and_reservation_data'] = 'Guest and reservation data';
    $lang['price_calculation'] = 'Price calculation';
    $lang['selected_facility'] = 'Selected facility';
    $lang['unit'] = 'Unit';
    $lang['fill_out_form'] = 'Please fill out the form to get an accurate price calculation';
    $lang['amount'] = 'Amount';
    $lang['no_nights'] = 'Number of nights';
    $lang['third_party'] = 'Bookme-montenegro.com agrees that the information entered on this site are used solely for the purpose of providing the
            requested service. Your personal information will be forwarded to a third party or service provider, for the
            purpose of recording reservations on your behalf. If it comes to your personal information changes or if
            there is an error when you type them, please notify us.';
    $lang['reservations'] = 'Reservations';
    $lang['directly'] = 'Call us directly';


    $lang['thanx_inquery_sent'] = 'Inquiry sent';
    $lang['thanx_text'] = 'Thank you for your inquiry<br><br>Our customer service will send you an e-mail as soon as possible with information about the availability of the accommodation, a price quotation and payment information. <br><br>Please check your email for inquiry confirmation.';

    $lang['thanx_realtime_sent'] = 'Reservation Confirmed';
    $lang['thanx_realtime_text'] = 'Thank you for your reservation<br><br>Please check your e-mail with the booking confirmation and instruction for payment. <br><br>Please make a payment within 72 hours under the selected payment method and dynamics. After your make a payment our customer service will send you VOUCHER and all information concerning your booking, such as the owner\'s name (contact person), accommodation address (or address at which you are applying on arrival) and phone number.';

    $lang['thanx_payment_text'] = 'Thank you for your reservation.<br><br>For any additional questions or information, please contact our customer service.<br><br>Thank you for booking with <b>bookme-montenegro.com</b>';

    
    $lang['no_view'] = 'No. views';
    $lang['contact_owner'] = 'Contact owner';

    /*
     * Excursions
     */
    $lang['exc.excursions'] = 'Excursions';
    $lang['exc.excursion'] = 'Excursion';
    $lang['exc.discover'] = 'Discover + Enjoy';

    /*
     * Transfers
     */
    $lang['transfer'] = 'Transfer';

// reviews
    $lang['reviews.total_score'] = "Total Score";
    $lang['reviews.leave_comment'] = "Leave a Comment & Rate";
    $lang['reviews.name'] = "Name";
    $lang['reviews.surname'] = "Surname";
    $lang['reviews.score'] = "Score";
    $lang['reviews.comment'] = "Comment";
    $lang['reviews.submit'] = "Submit";
    $lang['reviews.comments'] = "Comments";

    $lang['EUR'] = '&#128;';
    $lang['RUB'] = ' RUB ';
    $lang['USD'] = '$';

    $lang["pricelist"] = "Price list";
    $lang["mindays"] = "Min. Days";
    $lang["minpersons"] = "Min. Persons";

    $lang['dtl.tax_included'] = 'Residential tax is included in accommodation price.';
    $lang['dtl.tax_not_included'] = 'Residential tax and insurance are NOT included in the price and are being paid at check-in.';

    $lang["city_tax"] = "Residential tax";
    $lang["insurance_tax"] = "Insurance";
    $lang["eco_tax"] = "Eco tax";
    $lang["day"] = "day";

    $lang['in'] = 'in';

    $lang['listing'] = 'Showing';
    $lang['more_rooms'] = 'Show more rooms';
    $lang['hide_rooms'] = 'Hide rooms';
    $lang['important'] = 'Important information';

    $lang['personal_info'] = 'Personal information';
    $lang['contact_info'] = 'Contact information';
    $lang['spec_request'] = 'Special request';
    $lang['spec_req_text'] = 'Do you have any special requests for your stay? Your request will be received when you completed booking.';
    $lang['res_info'] = 'Reservation details';
    $lang['show_accomm'] = 'Show accommodation in';

    $lang['password'] = 'Password';
    $lang['password_again'] = 'Repeat password';
    $lang['password_not_mach'] = 'Password mismatch';
    $lang['login'] = 'Log in';
    $lang['logout'] = 'Log out';
    $lang['create_acc'] = 'Create account';
    $lang['acc_created'] = 'Account created';
    $lang['acc_type'] = 'Account type';
    $lang['register_property'] = 'Register your property';
    $lang['after_login'] = 'After log in, you will be able to register your property with';
    $lang['after_register'] = 'After registration has been completed, you will be able to register your property with';
    $lang['full_name'] = 'Full name';
    $lang['please_wait'] = 'Please wait';
    $lang['please_select'] = 'Please choose';
    $lang['loged_in'] = 'Loged in successfully';
    $lang['loged_out'] = 'Loged out successfully';
    $lang['loged_in_to_regiter'] = 'You must be loged in to register your property';
    $lang['error'] = 'Information';
    $lang['email_exists'] = 'The email address you have entered is already registered';
    $lang['login_incorect'] = 'Please enter the correct log in information';
    $lang['login_not_sent'] = 'Log in data not sent. Please try again';
    $lang['login_sent'] = 'Log in data sent at ';
    $lang['email_conf_now'] = 'You will receive email with confirmation data immediately';
    $lang['t_agency'] = 'Tourist agency';
    $lang['licence_ministry'] = 'Authorized permission of Ministry of Tourism for renting';
    $lang['licence_ministry_agency'] = 'Authorized licence of Ministry of Tourism';
    $lang['select_file'] = 'Select File';
    $lang['property_name'] = 'Property name';
    $lang['select_acc_type'] = 'Please select \'Account type\'';
    $lang['select_licence'] = 'Please upload Authorized permission of Ministry of Tourism for renting';
    $lang['property_registered'] = 'Property registered';
    $lang['file_uploaded'] = 'File uploaded';

    $lang['register_text'] = 'Register your property with bookme-montenegro.com the largest accommodation selection in montenegro';

    $lang['secure.guarantee'] = "Your Reservation Guarantee";
    $lang['secure.credit_info'] = "No booking fees! Your credit card is needed to guarantee your booking.";
    $lang['secure.card_type'] = "Credit card type";
    $lang['secure.card_number'] = "Credit card number";
    $lang['secure.card_holder_name'] = "Card holder's name";
    $lang['secure.exp_date'] = "Expiration date";
    $lang['secure.cvc'] = "CVC-code";


    /* -----------------------------------------------------------------------
      Excursions
      ----------------------------------------------------------------------- */
    $lang['exc_categories'] = 'Categories';
    $lang['exc_category'] = 'Category';
    $lang['exc_intro'] = 'Looking for adventure, natural beauty and a unique itinerary in <strong>Montenegro</strong>, contact <strong>cipa-booking.com</strong>. We offer you a large selection of excursions carefully designed to meet all your expectations and bring your travel fantasies to life. Our most captivating excursions itineraries will take you to the places you never dreamed of visiting. Let us show you <strong>Montenegro</strong> in a new and adventurous way.';
    $lang['per_person'] = 'per person';
    $lang['start_day'] = 'Departure day';
    $lang['guides'] = 'Guides';
    $lang['transportation_type'] = 'Transportation type';
    $lang['addition'] = 'Addition';
    $lang['pickup_location'] = 'Pickup Location';
    $lang['description'] = 'Description';
    $lang['exc_note'] = '<p>Prices are valid for summer season.</p><p>In the off season please contact agency for prices details.</p>';
    $lang['exc_similar'] = 'Similar excursions';
    $lang['exc_cal_instructions'] = 'Instructions: Click on  calendar to select the date of departure.';
    $lang['exc_departure_day'] = 'Please choose departure day';
    $lang['exc_service'] = 'Service';
    $lang['please_select'] = '-- Please select';
    $lang['week_days'] = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $lang['transports'] = array('Bus', 'Boat', 'Jeep', 'Bus + Boat', 'Bus + Jeep', 'Bus + Canoe');
    $lang['places_left'] = 'The remaining places ';
    $lang['exc_autocomp'] = 'History & Culture, Nature, Sport & Adventure';
    $lang['sellingSeason'] = 'Selling season: ';

    /* -----------------------------------------------------------------------
      Transfers
      ----------------------------------------------------------------------- */

    $lang['find_transfer'] = 'Find transfer';
    $lang['date'] = 'Date';
    $lang['date_return'] = 'Return date';
    $lang['time'] = 'Time';
    $lang['book_a_return'] = 'Book a return';
    $lang['ban_intro'] = 'Book your transfer on time, in just a few minutes, with a reliable partner';
    $lang['ban_1'] = 'No waiting in line for a taxi';
    $lang['ban_2'] = 'Simple, fast and reliable booking';
    $lang['ban_3'] = 'Attractive confirmed price with no hidden costs';
    $lang['ban_4'] = 'Air-conditioned vehicles with modern equipment';
    $lang['ban_5'] = 'Payment by credit card or bank transfer';
    $lang['ban_6'] = 'Your professional driver will welcome you at the airport gate with your name written on a sign';
    $lang['vehicle'] = 'Vehicle';
    $lang['price_per_transfer'] = 'Price per transfer';
    $lang['transfer_info'] = 'Transfer information';
    $lang['transfer_info_return'] = 'Return transfer information';
    $lang['flight_number'] = 'Flight number';
    $lang['arriving_from'] = 'Arriving from';
    $lang['airline_name'] = 'Airline name';
    $lang['from_hotel'] = 'From hotel';
    $lang['from_address'] = 'From address';
    $lang['to_hotel'] = 'To hotel';
    $lang['to_address'] = 'To address';
    $lang['hotel_or_property'] = 'Hotel or property';
    