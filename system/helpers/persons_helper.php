<?php

    function persons_plural($maxPS, $single, $plural, $lang) {

        $person_s = $maxPS > 1 ? $plural : $single;

        if ($lang == 'me'):

            if ($maxPS == 1)
                $person_s = $single;
            else if ($maxPS >= 2 && $maxPS <= 4)
                $person_s = lang('res.persone');
            else
                $person_s = $plural;

        endif;

        return $person_s;
    }
    
    function persons_plural_new($maxPS, $single, $plural, $lang) {

        $person_s = $maxPS > 1 ? $plural : $single;

        if ($lang == 'me'):

            if ($maxPS == 1)
                $person_s = lang('person_one');
            else if ($maxPS >= 2 && $maxPS <= 4)
                $person_s = $plural;
            else
                $person_s = $single;

        endif;

        return $person_s;
    }
    